"use strict";

// The selector html element which contains our products.
const productsSelector = document.getElementById("products-select");

// The four components' display components.
const bankBalanceView = document.getElementById("bank-balance");
const outstandingLoanView = document.getElementById("outstanding-loan");
const outstandingLoanField = document.getElementById("loan-outstanding-field");
const payBalanceView = document.getElementById("pay-balance");
const productView = document.getElementById("product");

// Buttons for the views.
const loanBtn = document.getElementById("loan-btn");
const bankBtn = document.getElementById("bank-btn");
const workBtn = document.getElementById("work-btn");
const downpayBtn = document.getElementById("downpay-btn");
const buyBtn = document.getElementById("buy-btn");

// API details
const baseURL = "https://hickory-quilled-actress.glitch.me/";
const computersEndPoint = "/computers";

// Session storage of the values.
let products = [];
let selectedProduct = {};
let bankBalance = 0;
let outstandingLoan = 0;
let payBalance = 0;

// Formatter for displaying currencies.
const formatter = new Intl.NumberFormat("nb-NO", { style: "currency", currency: "NOK" });

// Gets products from local storage if it is already set, else fetch from api.
const getProducts = async () => {
    let products = JSON.parse(localStorage.getItem("_products"));
    console.log(products);
    if(!products){
        products = await fetch(baseURL + computersEndPoint).then(result => result.json());
    }
    console.log(products);
    return products;
}

// Checks if set in local storage, else set to default (which is 0).
const getSelectedProduct = () => {
    let selectedProduct = localStorage.getItem("_selectedProduct");
    return selectedProduct ? JSON.parse(selectedProduct) : products[0];
}

// Loads data from local storage/ API into session.
const loadData = async () => {
    // localStorage.clear();

    products = await getProducts();
    selectedProduct = getSelectedProduct();
    payBalance = parseFloat(localStorage.getItem("_payBalance")) || 0;
    outstandingLoan = parseFloat(localStorage.getItem("_outstandingLoan")) || 0;
    bankBalance = parseFloat(localStorage.getItem("_bankBalance")) || 0;
}

// Saves data from session into local storage.
const saveData = () => {
    localStorage.setItem("_products", JSON.stringify(products));
    localStorage.setItem("_selectedProduct", JSON.stringify(selectedProduct));
    localStorage.setItem("_bankBalance", bankBalance);
    localStorage.setItem("_outstandingLoan", outstandingLoan);
    localStorage.setItem("_payBalance", payBalance);
}


// Updates view to show session values.
const updateViews = () => {
    updateOptionsView();
    updateBankBalanceView();
    updatePayBalanceView();
    updateOutstandingLoanView();
    updateSelectedProductView();
    updateDownpayBtnVisibility();
    updateLoanOutstandingVisibility();
}

// Updates options selector to view all product options.
const updateOptionsView = () => {
    if (products == null)
        throw new ReferenceError("Products are empty.");

    while(productsSelector.hasChildNodes()){
        productsSelector.removeChild(productsSelector.lastChild);
    }

    products.forEach((product, index) => {
        const newOption = document.createElement("option");
        newOption.setAttribute("value", index);
        newOption.textContent = product.title
        productsSelector.appendChild(newOption);
    });

    productsSelector.value = selectedProduct.id - 1; // Gotta offset the api products' id since product ids starts at 1...
}

// Updates bank balance view.
const updateBankBalanceView = () => {
    bankBalanceView.innerText = formatter.format(bankBalance);
}

// Updates pay balance view.
const updatePayBalanceView = () => {
    payBalanceView.innerText = formatter.format(payBalance);
}

// Updates outstanding loan view.
const updateOutstandingLoanView = () => {
    outstandingLoanView.innerText = formatter.format(outstandingLoan);
}

// Updates selected product view.
const updateSelectedProductView = () => {
    document.getElementById("product-img-url").setAttribute("src", baseURL + selectedProduct.image);
    document.getElementById("product-title").innerText = selectedProduct.title;
    document.getElementById("product-description").innerText = selectedProduct.description;
    document.getElementById("product-price").innerText = formatter.format( selectedProduct.price);
    document.getElementById("product-features").innerText = [...(selectedProduct.specs)].join("\n");
}


// Helper methods.

// Initializes the views after loading the required data.
const initializeViews = async () => {
    await loadData();
    updateViews(); 
}

// Selects a product by retrieving its index from looking at the event target value.
const selectProduct = (event) => {
    selectedProduct = products[event.target.value];
}

// When work button is hit this method will increase the pay balance.
const increasePayBalance = () => {
    payBalance += 100;
}

// Shows the product buy component. 
const showDownpayBtn = () => {
    downpayBtn.style.visibility = "block";
}

// Hides to product buy component.
const hideDownpayBtn = (_) => {
    downpayBtn.style.visibility = "none";
}

// Downpays with the current pay balance.
const downpay = () => {
    if (outstandingLoan > 0) {
        outstandingLoan -= payBalance;
        payBalance = 0;
        if(outstandingLoan < 0){
            payBalance = 0 - outstandingLoan;
        }
    }
}

// Presents the modal.
const presentModal = (_title, _desc, onOk = ()=>{console.log("default behaviour")}) => {
    let backdrop = document.createElement("div");
    backdrop.id = "presenting-modal"
    backdrop.style.position = "absolute";
    backdrop.style.left = "0";
    backdrop.style.right = "0";
    backdrop.style.zIndex = "1";
    backdrop.style.width = "100vw";
    backdrop.style.height = "100vh";
    backdrop.style.backdropFilter = "blur(10px)";
    backdrop.style.display = "flex";

    let modal = document.createElement("div"); 
    modal.setAttribute("class", "px-3"); 
    modal.style.backgroundColor = "white";
    modal.style.height = "25%";
    modal.style.maxHeight = "450px";
    modal.style.width = "50%"; 
    modal.style.minWidth = "250px"; 
    modal.style.maxWidth = "500px"; 
    modal.style.margin = "auto"; 
    modal.style.display = "flex";
    modal.style.flexDirection = "column";
    modal.style.justifyContent = "space-around";
    modal.style.borderRadius = "5%";
    modal.style.boxShadow = "0 5px 10px";

    let title = document.createElement("h2");
    title.setAttribute("class", "h2  ");
    title.innerText = _title; 
    
    let dismissBtn = document.createElement("button");
    dismissBtn.setAttribute("class", "btn text-black-50");
    dismissBtn.innerText = "X";
    
    let description = document.createElement("p");
    description.setAttribute("class", "text-black");
    description.innerText = _desc;
    
    let okBtn = document.createElement("button");
    okBtn.setAttribute("class", "btn btn btn-success");
    okBtn.innerText = "Ok"; 
    
    let header = document.createElement("div"); 
    header.style.display = "flex";
    header.style.flexFlow = "row nowrap";
    header.style.justifyContent = "space-between";
    header.appendChild(title);
    header.appendChild(dismissBtn);
    
    let body = document.createElement("div"); 
    body.style.display = "flex";
    body.appendChild(description);
    
    let footer = document.createElement("div"); 
    footer.style.display = "flex";
    footer.style.flexFlow = "row-reverse nowrap";
    footer.style.justifyContent = "flex-"; 
    footer.appendChild(okBtn);

    modal.appendChild(header);
    modal.appendChild(body);
    modal.appendChild(footer); 

    backdrop.appendChild(modal);

    document.body.prepend(backdrop);

    const disposeModal = () => {
        document.body.removeChild(document.getElementById("presenting-modal"));
    }

    dismissBtn.addEventListener("click", disposeModal);

    okBtn.addEventListener("click", onOk);
    okBtn.addEventListener("click", disposeModal);
}

const updateDownpayBtnVisibility = () => {
    if (outstandingLoan > 0 && payBalance > 0) {
        downpayBtn.style.display = "block";
    } else {
        downpayBtn.style.display = "none";
    }
}

const updateLoanOutstandingVisibility = () => {
    if (outstandingLoan > 0) {
        outstandingLoanView.style.display = "block";
        outstandingLoanField.style.display = "block";
    } else {
        outstandingLoanView.style.display = "none";
        outstandingLoanField.style.display = "none";
    }
}

const bankTransfer = () => {
    // Tries to transfer 0
    if (payBalance <= 0) {
        alert("Cant transfer if balance is zero.");
    // Tries to transfer and has loan, 10% needs to pay down, rest goes to bank
    } else if (outstandingLoan > 0) {
        outstandingLoan -= payBalance * 0.1;
        payBalance -= payBalance * 0.1;
        // Paid too much
        if (outstandingLoan < 0) {
            payBalance += 0 - parseFloat(outstandingLoan);
            outstandingLoan = 0;
        }
    }
    bankBalance += parseFloat(payBalance);
    payBalance = 0;
}

const makeLoan = () => { 
    let loanAmount = prompt("Please enter the amount you wish to loan: ");
    if (isNaN(loanAmount)) {
        alert("The value is not a number.")
    } else if (loanAmount <= 0) {
        alert("Value can't be less than or equal to zero.")
    } else if (outstandingLoan > 0) { 
        alert("Can't make another loan while one is already active.")
    }else if (loanAmount > bankBalance * 2){
        alert("Can't make loan bigger than double of bank balance.")
    }
    else {
        bankBalance += parseFloat(loanAmount);
        outstandingLoan = parseFloat(loanAmount);
        alert("Loan made.")
    }
}

const tryBuy = () => {
    if(bankBalance >= selectedProduct.price){
        presentModal("Checkout", "Are you sure you wish to buy " + selectedProduct.title + "?", ()=>alert("Payment successful."));
        bankBalance -= selectedProduct.price
    }else{
        presentModal("Not enough funds", "You do not have enough money to buy " + selectedProduct.title + "." );
    }
}

// Initializes values from local storage or retrieves from API based on availability.
addEventListener("DOMContentLoaded", initializeViews); 
addEventListener("beforeunload", saveData);

// Adds listeners to the selected element
productsSelector.addEventListener("change", selectProduct); 
productsSelector.addEventListener("change", updateViews); 

workBtn.addEventListener("click", increasePayBalance); 
workBtn.addEventListener("click", updateDownpayBtnVisibility);
workBtn.addEventListener("click", updateViews);

bankBtn.addEventListener("click", bankTransfer);  
bankBtn.addEventListener("click", updateDownpayBtnVisibility)
bankBtn.addEventListener("click", updateLoanOutstandingVisibility) 
bankBtn.addEventListener("click", updateViews) 

buyBtn.addEventListener("click", tryBuy);
buyBtn.addEventListener("click", updateBankBalanceView);

loanBtn.addEventListener("click", makeLoan); 
loanBtn.addEventListener("click", updateLoanOutstandingVisibility);
loanBtn.addEventListener("click", updateDownpayBtnVisibility);
loanBtn.addEventListener("click", updateViews);

downpayBtn.addEventListener("click", downpay);
downpayBtn.addEventListener("click", updateViews);