# Assignment one: Web app with VanillaJS.

## Description
For this assignment we had to create a web application managing bank loans, work payments and for buying laptops using vanilla javascript.
The key requirements are as follows:
- A user should be able to: 
    - View his bank balance
    - Make loans 
    - Downpay their loan 
    - Do work 
    - Transfer pay balance to bank balance 
    - Buy computers
- The following restrictions are in effect:
    - A user cannot make a loan if the requested amount is more than double of their bank balance.
    - A user cannot downpay if it has no outstanding loan
    - A user can only make **one** loan at a time
    - If a user transfers his pay balance to his bank balance, 10% of the amount should be diverted towards downpayment of a loan
    
## Result
The assignment is completed and has been deployed [here](https://musical-beignet-261e8b.netlify.app/).

A graph visualization of the general program flow:
```mermaid
stateDiagram-v2
[*] --> start
start --> makeLoan: if outstanding loan == 0
start --> bankTransfer: if paybalance != 0
start --> dowork
start --> downpay
start --> buy
makeLoan--> loanError
makeLoan --> loanSuccess
loanSuccess --> wait 
loanError-->wait 
bankTransfer --> transferSuccess
bankTransfer --> transferError
transferSuccess--> wait 
transferError--> wait 
downpay --> downpayError
downpay --> downpaySuccess
downpaySuccess --> wait
downpayError --> wait
buy --> buyError
buy --> buySuccess
buyError --> wait
buyuccess --> wait
dowork --> wait 
wait --> [*]: Exit
wait--> start: Continue
```